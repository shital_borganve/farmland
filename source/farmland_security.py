#!/usr/bin/python
import cv2
import os 
import threading
import thread
import time
import RPi.GPIO as GPIO
from subprocess import call
import subprocess
import datetime
from lcd import *
import dht11
from serial import *
from GSMsms import TextMessage
from  face_detect import Find_Faces
#from sprayUnit import *
from time import sleep

LCD_RS = 7
LCD_E  = 8
LCD_D4 = 25
LCD_D5 = 24
LCD_D6 = 23
LCD_D7 = 18

Motor1A = 26        #dc motor
Motor1B = 21 

BUZZER = 20 
LCD_WIDTH = 16    # Maximum characters per line
LCD_CHR = True
LCD_CMD = False
 
LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line
 
# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005
GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM)
GPIO.setup(LCD_E, GPIO.OUT)  # E
GPIO.setup(LCD_RS, GPIO.OUT) # RS
GPIO.setup(LCD_D4, GPIO.OUT) # DB4
GPIO.setup(LCD_D5, GPIO.OUT) # DB5
GPIO.setup(LCD_D6, GPIO.OUT) # DB6
GPIO.setup(LCD_D7, GPIO.OUT) # DB7
GPIO.setup(BUZZER, GPIO.OUT) # DB7

GPIO.setup(Motor1A,GPIO.OUT)  #dc motor
GPIO.setup(Motor1B,GPIO.OUT)
 
PIR_1=15
PIR_2=17
PIR_3=27
PIR_4=22
PIR_5=10
PIR_6=9
GPIO.setup(PIR_1, GPIO.IN)         #Read output from PIR motion sensor 1
GPIO.setup(PIR_2, GPIO.IN)         #Read output from PIR motion sensor 2
GPIO.setup(PIR_3, GPIO.IN)         #Read output from PIR motion sensor 3
GPIO.setup(PIR_4, GPIO.IN)         #Read output from PIR motion sensor 4
GPIO.setup(PIR_5, GPIO.IN)         #Read output from PIR motion sensor 5
GPIO.setup(PIR_6, GPIO.IN)         #Read output from PIR motion sensor 6

Phone_No = "+919916395250"

GPIO.setup(14, GPIO.IN)
instance = dht11.DHT11(pin = 14)


lcd_init()
lock = threading.Lock()
lcd_string("Farmland Security",LCD_LINE_1)
lcd_string("System Booted",LCD_LINE_2)
time.sleep(3)

def video_record(threadName, delay,lock):
  #avconv -loglevel quiet -f video4linux2 -r 10  -t 00:00:10 -i /dev/video0 test.avi # to record video
  print "Video record thread function called"
  videoFile = '/home/pi/farmland_security/videos/farmVideo-%s.mp4'%datetime.datetime.now().strftime('%Y-%m-%d_%H-%M') 
  dropboxFile= 'farmVideo-%s.mp4'%datetime.datetime.now().strftime('%Y-%m-%d_%H-%M') 
  recordVideo = "avconv -loglevel quiet -f video4linux2 -r 10  -t 00:00:05 -i /dev/video0 " + str(videoFile)   
  call ([recordVideo], shell=True)
  #Upload file to Dropbox

  UploadVid = "bash ./dropbox_uploader.sh upload " + str(videoFile) +" "+ str(dropboxFile)
  call ([UploadVid], shell=True)
  print "Video uploaded to Dropbox"
"""  
def LCD_Display(threadName, delay,lock):
  while True:
    result = instance.read()
    lock.acquire()
    lcd_byte(0x01, LCD_CMD)
    if result.is_valid():    
      print("Temperature: %d C" % result.temperature)
      print("Humidity: %d %%" % result.humidity)  
      lcd_string("Temperature:%d C"% result.temperature,LCD_LINE_1)
      lcd_string("Humidity: %d %%"% result.humidity,LCD_LINE_2)
    else:
      lcd_string("Temp Read Err",LCD_LINE_1)
      print result
    time.sleep(5)
    lock.release()
    #print "************Inside LCD function********"
""" 

def EggSpray(vTime):
  #print "Turning motor on"
  GPIO.output(Motor1A,GPIO.HIGH)
  GPIO.output(Motor1B,GPIO.LOW)

  sleep(vTime)
  #print "Stopping motor"
  GPIO.output(Motor1A,GPIO.LOW)
  GPIO.output(Motor1B,GPIO.LOW)
   
def LCD_Display():
    result = instance.read()
    lcd_byte(0x01, LCD_CMD)
    if result.is_valid():    
      print("Temperature: %d C" % result.temperature)
      print("Humidity: %d %%" % result.humidity)  
      lcd_string("Temperature:%d C"% result.temperature,LCD_LINE_1)
      lcd_string("Humidity: %d %%"% result.humidity,LCD_LINE_2)
    else:
      lcd_string("Temp Read Err",LCD_LINE_1)
      print result
    
#print "Create LCD thread"   
#thread.start_new_thread(LCD_Display, ("DISPLAY STRING", 2,lock ) )
  
while True:

  P1=GPIO.input(PIR_1)
  P2=GPIO.input(PIR_2)
  P3=GPIO.input(PIR_3)
  P4=GPIO.input(PIR_4)
  P5=GPIO.input(PIR_5)
  P6=GPIO.input(PIR_6)
  LCD_Display()     
  if P1+P2+P3+P4+P5+P6!=0:  
    #Take picture and video 
    #fswebcam -r 640x480 --jpeg 85 -D 1 shot.jpg # To Capture image
    print "Capture pic File"
    picFile = '/home/pi/farmland_security/pics/farmPic-%s.jpg'%datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')
    dropboxPic = 'farmPic-%s.jpg'%datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')
    capturePic = "fswebcam -r 640x480 --jpeg 85 -D 1 " + str(picFile)   
    call ([capturePic], shell=True)

    UploadPic = "bash ./dropbox_uploader.sh upload " + str(picFile) +" "+ str(dropboxPic)
    call ([UploadPic], shell=True)
    print "PIC uploaded to Dropbox"


    print "Create Record thread"
    thread.start_new_thread(video_record, ("RECORDING VIDEO", 2,lock ) )
    lcd_byte(0x01, LCD_CMD)
    #lock.acquire()
    lcd_string("Waiting for RFID scan...",LCD_LINE_1)
    ser = Serial("/dev/ttyUSB0", baudrate=9600, timeout=5)
    data = ser.read(8)
    if data:
      print data
      lcd_byte(0x01, LCD_CMD)
      lcd_string("Auth Person",LCD_LINE_1)
      lcd_string("Entry Permitted",LCD_LINE_2)
      sms = TextMessage(Phone_No,"Authorised Entry To Farmland")
      sms.connectPhone()
      sms.sendMessage()
      sms.disconnectPhone()
      time.sleep(2)
      lcd_byte(0x01, LCD_CMD)
      lcd_string("Message sent...", LCD_LINE_1)
      print "message sent successfully"
    else:
      #Image processing....
      sms = TextMessage(Phone_No,"Unauthorised Entry To Farmland")
      sms.connectPhone()
      sms.sendMessage()
      sms.disconnectPhone()
      lcd_byte(0x01, LCD_CMD)
      lcd_string("Message sent!!!", LCD_LINE_1)
      print "message sent successfully"
      time.sleep(2)
      lcd_string("Unauthorised person",LCD_LINE_1)
      GPIO.output(BUZZER, True)
      time.sleep(3)
      GPIO.output(BUZZER, False)
      #image processing.....
      print "Finding Human or Animal.....!"
     
      val = Find_Faces(picFile)
      print "Image processed...."
      print val
      if val:
	
          
      	#from captured image check if human or animal
      	#if zero faces detected then animal els human
      	#if unauthorised human then ON buzzer
      	print "Unauthorised human intruder"
	sms = TextMessage(Phone_No,"intrusion:Human..")
        sms.connectPhone()
        sms.sendMessage()
        sms.disconnectPhone()
 
      	GPIO.output(BUZZER, True)
      	time.sleep(3)
      	GPIO.output(BUZZER, False)
      else:
        lcd_byte(0x01, LCD_CMD)
	print P1, P2, P3, P4, P5, P6
      	#intrusion by animal
      	print"Intrusion by animal"
	#Else Look for small/Big Animal
      	#condition for small animal
      	#if (P1!=0 or P1*P2!=0 or P1*P2*P3!=0) and P4+P5+P6==0:
      	if (P1!=0 or P1*P2!=0 or P1*P2*P3!=0):
            print "Small Animal Intrusion"
            sms = TextMessage(Phone_No,"intrusion:small animal")
            sms.connectPhone()
	    sms.sendMessage()
            sms.disconnectPhone()

	    print "dc motor turned ON"
	    EggSpray(4)
	    """
	    GPIO.setup(Motor1A,GPIO.HIGH)  #dc motor
            GPIO.setup(Motor1B,GPIO.LOW)
	    time.sleep(3)
	    GPIO.setup(Motor1A,GPIO.LOW)  #dc motor
            GPIO.setup(Motor1B,GPIO.LOW)
	    """
            print "dc motor turned OFF"
                    
        if (P4!=0 or P4*P5!=0 or P4*P5*P6!=0):
            print "Big Animal Intrusion"
            sms = TextMessage(Phone_No,"intrusion:big animal")
            sms.connectPhone()
	    sms.sendMessage()
            sms.disconnectPhone()
            print"Firecracker Turned ON"
	    os.system("aplay ../firecracker.mp3 -d 10")
            print"Firecracker Turned OFF" 

  P1=P2=P3=P4=P5=P6=0
  time.sleep(3)
  print "Loop Back......" 

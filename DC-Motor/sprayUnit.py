import RPi.GPIO as GPIO
from time import sleep
 
GPIO.setmode(GPIO.BCM)
 
Motor1A = 26 
Motor1B = 21 
GPIO.setup(Motor1A,GPIO.OUT)  #dc motor
GPIO.setup(Motor1B,GPIO.OUT)

print "Turning motor on"
GPIO.output(Motor1A,GPIO.HIGH)
GPIO.output(Motor1B,GPIO.LOW)
 
sleep(3)
print "Stopping motor"
GPIO.output(Motor1A,GPIO.LOW)
GPIO.output(Motor1B,GPIO.LOW)

import threading
import thread
import time
import RPi.GPIO as GPIO
from subprocess import call
import datetime
import os


picFile = '/home/pi/farmland_security/pics/farmPic-%s.jpg'%datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')

print picFile
capturePic = "fswebcam -r 640x480 --jpeg 85 -D 1 " + str(picFile)

print capturePic
call ([capturePic], shell=True)


videoFile = '/home/pi/farmland_security/videos/farmVideo-%s.mp4'%datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')
dropboxFile = 'farmVideo-%s.mp4'%datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')
print videoFile
print dropboxFile
recordVideo = "avconv -loglevel quiet -f video4linux2 -r 10  -t 00:00:05 -i /dev/video0 " + str(videoFile)
print recordVideo
call ([recordVideo], shell=True)
#Upload file to Dropbox
#UploadVid = "bash ./dropbox_uploader.sh upload " + str(videoFile) +" "+ str(dropboxFile)
print UploadVid
call ([UploadVid], shell=True)
print "Video uploaded to Dropbox"


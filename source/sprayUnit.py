import RPi.GPIO as GPIO
from time import sleep

Motor1A = 37        #dc motor
Motor1B = 40

 
def EggSpray(vTime): 
  GPIO.setup(Motor1A,GPIO.OUT)  #dc motor
  GPIO.setup(Motor1B,GPIO.OUT)
  print "Turning motor on"
  GPIO.output(Motor1A,GPIO.HIGH)
  GPIO.output(Motor1B,GPIO.LOW)
 
  sleep(vTime)
  print "Stopping motor"
  GPIO.output(Motor1A,GPIO.LOW)
  GPIO.output(Motor1B,GPIO.LOW)
 

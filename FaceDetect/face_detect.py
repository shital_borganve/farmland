import cv2
import sys
#import cv2.cv as cv
import numpy as np
# Get user supplied values
def Find_Faces(imagePath):
  cascPath = 'haarcascade_frontalface_default.xml' 
  # Create the haar cascade
  faceCascade = cv2.CascadeClassifier(cascPath)

  # Read the image
  image = cv2.imread(imagePath)
  gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

  # Detect faces in the image
  faces = faceCascade.detectMultiScale( gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30), flags = cv2.CASCADE_SCALE_IMAGE)
  val = len(faces)
  return val 
